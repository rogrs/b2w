package br.com.rogrs.b2w.desafio.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.rogrs.b2w.desafio.domain.Planetas;
import br.com.rogrs.b2w.desafio.repository.PlanetasRepository;
import br.com.rogrs.b2w.desafio.service.SWAPIService;
import br.com.rogrs.b2w.desafio.web.rest.util.HeaderUtil;
import br.com.rogrs.b2w.desafio.web.rest.util.PaginationUtil;
import br.com.rogrs.b2w.desafio.web.rest.util.ResponseUtil;

@RestController
@RequestMapping("/api")
public class PlanetasResource {

    private final Logger log = LoggerFactory.getLogger(PlanetasResource.class);

    private static final String ENTITY_NAME = "planetas";

    @Autowired
    private final PlanetasRepository planetasRepository;
    
    @Autowired
    private SWAPIService swAPIService;

    public PlanetasResource(PlanetasRepository planetasRepository, SWAPIService swAPIService) {
        this.planetasRepository = planetasRepository;
        this.swAPIService=swAPIService;
    }

    /**
     * POST  /planetas : Adiciona um novo planeta.
     *
     * @param planetas the planetas to create
     * @return the ResponseEntity with status 201 (Created) and with body the new planetas, or with status 400 (Bad Request) if the planetas has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/planetas")
    public ResponseEntity<Planetas> createPlanetas(@Valid @RequestBody Planetas planetas) throws URISyntaxException {
        log.debug("REST request to save Planetas : {}", planetas);
     
        planetas.setNumeroFilmes(swAPIService.obterQuantidadeAparicoesFilmes(planetas.getNome()));
        
        Planetas result = planetasRepository.save(planetas);
        return ResponseEntity.created(new URI("/api/planetas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /planetas : Updates an existing planetas.
     *
     * @param planetas the planetas to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated planetas,
     * or with status 400 (Bad Request) if the planetas is not valid,
     * or with status 500 (Internal Server Error) if the planetas couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/planetas")
    public ResponseEntity<Planetas> updatePlanetas(@Valid @RequestBody Planetas planetas) throws URISyntaxException {
        log.debug("REST request to update Planetas : {}", planetas);
        if (planetas.getId() == null) {
            return createPlanetas(planetas);
        }else {
        	planetas.setNumeroFilmes(swAPIService.obterQuantidadeAparicoesFilmes(planetas.getNome()));
        }
        Planetas result = planetasRepository.save(planetas);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, planetas.getId().toString()))
            .body(result);
    }

    /**
     * GET  /planetas : Lista todos os planetas com paginação
     *
     * @param pageable informações de paginação
     * @return the ResponseEntity with status 200 (OK) and the list of planetas in body
     */
    @GetMapping("/planetas")
    public ResponseEntity<List<Planetas>> getAllPlanetas(Pageable pageable) {
        log.debug("REST request to get a page of Planetas");
        Page<Planetas> page = planetasRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/planetas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /planetas/:id : Busca planeta por "id".
     *
     * @param id o id do planeta para recuperar
     * @return the ResponseEntity with status 200 (OK) and with body the planetas, or with status 404 (Not Found)
     */
    @GetMapping("/planetas/{id}")
    public ResponseEntity<Optional<Planetas>> getPlanetas(@PathVariable Long id) {
        log.debug("REST request to get Planetas : {}", id);
        Optional<Planetas> planetas = planetasRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(planetas));
    }
    
    
    /**
     * GET  /planetas/buscarNome/:nome : Buscar planeta por "nome" .
     *
     * @param nome o nome do planeta para recuperar
     * @return the ResponseEntity with status 200 (OK) and with body the planetas, or with status 404 (Not Found)
     */
    @GetMapping("/planetas/buscarNome/{nome}")
    public ResponseEntity<Optional<Planetas>> buscarPorNome(@PathVariable String nome) {
        log.debug("REST request to get Planeta buscarPorNome : {}", nome);
        Optional<Planetas> planetas = planetasRepository.findByNome(nome);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(planetas));
    }
    
   

    /**
     * DELETE  /planetas/:id : Excluir planeta por "id".
     *
     * @param id o id para excluir o planeta
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/planetas/{id}")
    public ResponseEntity<Void> deletePlanetas(@PathVariable Long id) {
        log.debug("REST request to delete Planetas : {}", id);
        planetasRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, String.valueOf(id))).build();
    }
}
