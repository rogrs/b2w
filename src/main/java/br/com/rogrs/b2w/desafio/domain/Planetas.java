package br.com.rogrs.b2w.desafio.domain;


import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;



@Entity
@Table(name = "planetas",indexes = {@Index(name = "idxnomeplaneta",  columnList="nome", unique = false)})
public class Planetas implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Size(max = 30)
    @Column(name = "nome", length = 30, nullable = false)
    private String nome;

    @Size(max = 30)
    @Column(name = "clima", length = 30)
    private String clima;

    @Size(max = 50)
    @Column(name = "terreno", length = 50)
    private String terreno;
    
    @Column(name = "numerofilmes")
    private int numeroFilmes;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public Planetas nome(String nome) {
        this.nome = nome;
        return this;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getClima() {
        return clima;
    }

    public Planetas clima(String clima) {
        this.clima = clima;
        return this;
    }

    public void setClima(String clima) {
        this.clima = clima;
    }

    public String getTerreno() {
        return terreno;
    }

    public Planetas terreno(String terreno) {
        this.terreno = terreno;
        return this;
    }

    public void setTerreno(String terreno) {
        this.terreno = terreno;
    }
    
    public int getNumeroFilmes() {
		return numeroFilmes;
	}

	public void setNumeroFilmes(int numeroFilmes) {
		this.numeroFilmes = numeroFilmes;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Planetas planetas = (Planetas) o;
        if (planetas.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), planetas.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Planetas{" +
            "id=" + getId() +
            ", nome='" + getNome() + "'" +
            ", clima='" + getClima() + "'" +
            ", terreno='" + getTerreno() + "'" +
            ", numeroFilmes='" + getNumeroFilmes() + "'" +
            "}";
    }

	
}