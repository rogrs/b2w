package br.com.rogrs.b2w.desafio.service;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

@Service
public class SWAPIService {

	private final Logger log = LoggerFactory.getLogger(SWAPIService.class);
	
	private static final String SEM_EXIBICAO_DE_FILMES = "[]";
	
	private static final String SEM_PROXIMA_PAGINA = "null";

	@Value("${api.server.base_uri}")
	private String baseApiUri;

	private String obterDadosSWAPI(String uri) {
		HttpResponse<String> response = null;
		try {
			response = Unirest.get(uri).header("Cache-Control", "no-cache").asString();
			return response.getBody();
		} catch (UnirestException ex) {
			log.error(String.format("UnirestException  message [%s]", ex.getMessage()), ex);
			return null;
		}
	}

	private int buscarPlanetaSWAPI(final String nomePlaneta, final String uri) {
		int numeroFilmes = 0;
		JSONObject jsonObject;

		String response = obterDadosSWAPI(uri);
		try {
			jsonObject = new JSONObject(response);
			JSONArray array = jsonObject.getJSONArray("results");
			String proximaURI = jsonObject.optString("next", null);

			for (int i = 0; i < array.length(); i++) {
				if (array.getJSONObject(i).optString("name", null).equalsIgnoreCase(nomePlaneta)) {
					String[] filmes = array.getJSONObject(i).optString("films").split(",");

					for (String filme : filmes) {
						//Alguns planetas não possuem exibições em filmes por isso tive que fazer esse tratamento
						if (filme.equalsIgnoreCase(SEM_EXIBICAO_DE_FILMES)) {
							numeroFilmes = 0;
							break;
						} else {
							numeroFilmes++;
						}
					}
					return numeroFilmes;
				}
			}

			//Sai da recursividade se proximaURI tiver o valor texto 'null'
			if (!proximaURI.equalsIgnoreCase(SEM_PROXIMA_PAGINA)) {
				return buscarPlanetaSWAPI(nomePlaneta, proximaURI);
			}

		} catch (JSONException e) {
			log.error(String.format("JSONException  message [%s]", e.getMessage()), e);
		}
		return numeroFilmes;
	}

	public int obterQuantidadeAparicoesFilmes(final String nomePlaneta) {

		return buscarPlanetaSWAPI(nomePlaneta, baseApiUri);
	}

}
