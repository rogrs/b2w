package br.com.rogrs.b2w.desafio.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.rogrs.b2w.desafio.domain.Planetas;

@SuppressWarnings("unused")
@Repository
public interface PlanetasRepository extends JpaRepository<Planetas, Long> {

	public Optional<Planetas> findByNome(String nome);

}
