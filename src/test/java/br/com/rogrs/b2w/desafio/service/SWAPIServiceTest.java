package br.com.rogrs.b2w.desafio.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SWAPIServiceTest {

	@Autowired
	private SWAPIService swAPIService;
	
	private static final String PLANETA_SKAKO = "Skako";
	private static final String PLANETA_KAMINO = "Kamino";
	private static final String PLANETA_JAKKU = "Jakku";
	private static final String PLANETA_DESCONHECIDO = "Blabla";

	@Test
	public void testeBuscarSkakoSWAPISemExibicaoFilmesSucesso() throws Exception {
		int ExibicoesAlderaan = swAPIService.obterQuantidadeAparicoesFilmes(PLANETA_SKAKO);
		assertTrue(ExibicoesAlderaan==0);
	}

	@Test
	public void testeBuscarKaminoSWAPISucesso() throws Exception {
		int ExibicoesKamino = swAPIService.obterQuantidadeAparicoesFilmes(PLANETA_KAMINO);
		assertTrue(ExibicoesKamino > 0);
	}

	@Test
	public void testeBuscarJakkuSWAPISucesso() throws Exception {
		int ExibicoesJakku = swAPIService.obterQuantidadeAparicoesFilmes(PLANETA_JAKKU);
		assertTrue(ExibicoesJakku > 0);
	}

	@Test
	public void testeBuscarDadosSWAPIPlanetaNaoCadastrado() throws Exception {
		int ExibicoesBlabla = swAPIService.obterQuantidadeAparicoesFilmes(PLANETA_DESCONHECIDO);
		assertTrue(ExibicoesBlabla == 0);
	}

}
