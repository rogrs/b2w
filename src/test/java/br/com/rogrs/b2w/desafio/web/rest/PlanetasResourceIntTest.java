package br.com.rogrs.b2w.desafio.web.rest;

import static br.com.rogrs.b2w.desafio.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import br.com.rogrs.b2w.desafio.DesafioApplication;
import br.com.rogrs.b2w.desafio.domain.Planetas;
import br.com.rogrs.b2w.desafio.repository.PlanetasRepository;
import br.com.rogrs.b2w.desafio.service.SWAPIService;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DesafioApplication.class)
public class PlanetasResourceIntTest {

	private static final String DEFAULT_NOME = "Tatooine";
	private static final String UPDATED_NOME = "Alderaan";

	private static final String DEFAULT_CLIMA = "Arido";
	private static final String UPDATED_CLIMA = "Temperado";

	private static final String DEFAULT_TERRENO = "Deserto";
	private static final String UPDATED_TERRENO = "Pastagens, montanhas";

	@Autowired
	private PlanetasRepository planetasRepository;
	
	@Autowired
	private SWAPIService swAPIService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private EntityManager em;

	private MockMvc restPlanetasMockMvc;

	private Planetas planetas;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final PlanetasResource planetasResource = new PlanetasResource(planetasRepository,swAPIService);
		this.restPlanetasMockMvc = MockMvcBuilders.standaloneSetup(planetasResource)
				.setCustomArgumentResolvers(pageableArgumentResolver)
				.setConversionService(createFormattingConversionService()).setMessageConverters(jacksonMessageConverter)
				.build();
	}

	/**
	 * Cria uma entidade para testes.
	 *
	 * Este é um método estático, pois testes para outras entidades também podem
	 * precisarem disso, se testarem uma entidade que requer a entidade atual.
	 */
	public static Planetas createEntity(EntityManager em) {
		Planetas planetas = new Planetas().nome(DEFAULT_NOME).clima(DEFAULT_CLIMA).terreno(DEFAULT_TERRENO);
		return planetas;
	}

	@Before
	public void initTest() {
		planetas = createEntity(em);
	}

	@Test
	@Transactional
	public void createPlanetas() throws Exception {
		int databaseSizeBeforeCreate = planetasRepository.findAll().size();

		restPlanetasMockMvc.perform(post("/api/planetas").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(planetas))).andExpect(status().isCreated());

		List<Planetas> planetasList = planetasRepository.findAll();
		assertThat(planetasList).hasSize(databaseSizeBeforeCreate + 1);
		Planetas testPlanetas = planetasList.get(planetasList.size() - 1);
		assertThat(testPlanetas.getNome()).isEqualTo(DEFAULT_NOME);
		assertThat(testPlanetas.getClima()).isEqualTo(DEFAULT_CLIMA);
		assertThat(testPlanetas.getTerreno()).isEqualTo(DEFAULT_TERRENO);
	}

	@Test
	@Transactional
	public void checkNomeIsRequired() throws Exception {
		int databaseSizeBeforeTest = planetasRepository.findAll().size();
		planetas.setNome(null);

		restPlanetasMockMvc.perform(post("/api/planetas").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(planetas))).andExpect(status().isBadRequest());

		List<Planetas> planetasList = planetasRepository.findAll();
		assertThat(planetasList).hasSize(databaseSizeBeforeTest);
	}

	@Test
	@Transactional
	public void getAllPlanetas() throws Exception {
		planetasRepository.saveAndFlush(planetas);

		restPlanetasMockMvc.perform(get("/api/planetas?sort=id,desc")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(planetas.getId().intValue())))
				.andExpect(jsonPath("$.[*].nome").value(hasItem(DEFAULT_NOME.toString())))
				.andExpect(jsonPath("$.[*].clima").value(hasItem(DEFAULT_CLIMA.toString())))
				.andExpect(jsonPath("$.[*].terreno").value(hasItem(DEFAULT_TERRENO.toString())));
	}

	@Test
	@Transactional
	public void getPlanetasBuscaPorNome() throws Exception {

		planetasRepository.saveAndFlush(planetas);

		restPlanetasMockMvc.perform(get("/api/planetas/buscarNome/{nome}", planetas.getNome()))
				.andExpect(status().isOk()).andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(planetas.getId().intValue()))
				.andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
				.andExpect(jsonPath("$.clima").value(DEFAULT_CLIMA.toString()))
				.andExpect(jsonPath("$.terreno").value(DEFAULT_TERRENO.toString()));
	}

	@Test
	@Transactional
	public void getPlanetas() throws Exception {

		planetasRepository.saveAndFlush(planetas);

		restPlanetasMockMvc.perform(get("/api/planetas/{id}", planetas.getId())).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(planetas.getId().intValue()))
				.andExpect(jsonPath("$.nome").value(DEFAULT_NOME.toString()))
				.andExpect(jsonPath("$.clima").value(DEFAULT_CLIMA.toString()))
				.andExpect(jsonPath("$.terreno").value(DEFAULT_TERRENO.toString()));
	}

	@Test
	@Transactional
	public void updatePlanetas() throws Exception {
		planetasRepository.saveAndFlush(planetas);
		int databaseSizeBeforeUpdate = planetasRepository.findAll().size();

		Planetas updatedPlanetas = planetasRepository.findById(planetas.getId()).get();

		em.detach(updatedPlanetas);
		updatedPlanetas.nome(UPDATED_NOME).clima(UPDATED_CLIMA).terreno(UPDATED_TERRENO);

		restPlanetasMockMvc.perform(put("/api/planetas").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(updatedPlanetas))).andExpect(status().isOk());

		List<Planetas> planetasList = planetasRepository.findAll();
		assertThat(planetasList).hasSize(databaseSizeBeforeUpdate);
		Planetas testPlanetas = planetasList.get(planetasList.size() - 1);
		assertThat(testPlanetas.getNome()).isEqualTo(UPDATED_NOME);
		assertThat(testPlanetas.getClima()).isEqualTo(UPDATED_CLIMA);
		assertThat(testPlanetas.getTerreno()).isEqualTo(UPDATED_TERRENO);
	}

	@Test
	@Transactional
	public void updateNonExistingPlanetas() throws Exception {
		int databaseSizeBeforeUpdate = planetasRepository.findAll().size();

		restPlanetasMockMvc.perform(put("/api/planetas").contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(planetas))).andExpect(status().isCreated());

		List<Planetas> planetasList = planetasRepository.findAll();
		assertThat(planetasList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deletePlanetas() throws Exception {
		planetasRepository.saveAndFlush(planetas);
		int databaseSizeBeforeDelete = planetasRepository.findAll().size();

		restPlanetasMockMvc
				.perform(delete("/api/planetas/{id}", planetas.getId()).accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		List<Planetas> planetasList = planetasRepository.findAll();
		assertThat(planetasList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(Planetas.class);
		Planetas planetas1 = new Planetas();
		planetas1.setId(1L);
		Planetas planetas2 = new Planetas();
		planetas2.setId(planetas1.getId());
		assertThat(planetas1).isEqualTo(planetas2);
		planetas2.setId(2L);
		assertThat(planetas1).isNotEqualTo(planetas2);
		planetas1.setId(null);
		assertThat(planetas1).isNotEqualTo(planetas2);
	}
}
