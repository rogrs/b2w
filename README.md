# Projeto Desafio B2W Digital

### Requisitos de software
  
  * Java 8
  * Maven 3 ou superior

## Para iniciar o progama digite no terminal/console o comando abaixo:
    
    mvn spring-boot:run

 ou no linux

    ./run


## No navegador abra o seguinte link abaixo:

     http://localhost:8080

## Exemplos de requisições 

  No arquivo  [Desafio B2W Digital.postman_collection.json](https://bitbucket.org/rogrs/b2w/src/b72562fe0165568eaf5d16bc4f18585ec911c08c/Desafio%20B2W%20Digital.postman_collection.json?at=master&fileviewer=file-view-default) pode ser importando as requisições na ferramenta [Postman.](https://www.getpostman.com/)

  ou utilize os comandos abaixo no [CURL](https://curl.haxx.se/)
   
## Adicionar Planeta #

    curl -X POST \
    http://localhost:8080/api/planetas \
      -H 'Cache-Control: no-cache' \
      -H 'Content-Type: application/json' \
       -d '{
            "nome" : "Aderaan",
            "clima" : "quente",
            "terreno" : "deserto"
      }'

## Listar Planetas

    curl -X GET \
        http://localhost:8080/api/planetas \
          -H 'Cache-Control: no-cache' \
          -H 'Content-Type: application/json' \

## Buscar por ID planeta

    curl -X GET \
      http://localhost:8080/api/planetas/1 \
        -H 'Cache-Control: no-cache' \
        -H 'Content-Type: application/json' \

## Buscar por Nome de planeta

    curl -X GET \
      http://localhost:8080/api/planetas/buscarNome/Aderaan \
        -H 'Cache-Control: no-cache' \
        -H 'Content-Type: application/json' \


## Excluir planeta por ID

    curl -X DELETE \
        http://localhost:8080/api/planetas/1 \
        -H 'Cache-Control: no-cache' \
